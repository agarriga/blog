+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5
+++

I am a second-year PhD student in machine learning advised by [Prof. Carl
Rasmussen](http://mlg.eng.cam.ac.uk/carl/) at the University of Cambridge. My
research interests are in uncertainty-aware Bayesian machine learning
algorithms. More concretely, I focus on time-series models for reinforcement
learning (RL), based on Gaussian processes. I'm also interested in safety,
interpretability, RL in general, and Bayesian neural networks.

I hold a MSc in Computer Science from the University of Oxford. While an
undergraduate at [Universitat Pompeu
Fabra](https://www.upf.edu/web/ai-ml/about-us) in Barcelona, I co-founded
[MonkingMe](https://monkingme.com/), an online music distribution company.

I am a co-organiser of the [ICLR 2019
workshop](https://sites.google.com/view/safeml-iclr2019) "Safe Machine Learning:
Specification, Robustness and Assurance".

I run the [Engineering Safe AI reading group](https://valuealignment.ml/) about technical approaches to ensuring AI has a positive impact in society, as part of Effective Altruism Cambridge.
