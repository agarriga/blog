+++
date = "2016-02-03T23:28:01+01:00"
draft = true
title = "Frictionless OS X screenshot sharing with Nyazo"

tags = ["scripting", "OS X"]
categories = ["Tutorials"]

+++

OS X has a nice screenshot feature. It's fast and easy to use: just press
`Cmd-Shift-4` and select your shot, which will appear in the desktop. By
pressing space, you can switch to window shot.

The problems come when you want to share your screenshot on, say, IRC. Direct
transfer (DCC) is usually too much hassle for that, and some clients do not
support it. What you usually do is upload the picture to the web, and link it.

You could just manually upload the file the screenshot feature produces. But I'm
spoiled by the use of [Nyazo](http://nyazo.jp/) on Linux, and I wanted better.

So in a few minutes you can tweak Nyazo's Linux script and end up with this:
```sh
#!/bin/sh
screencapture $1 /tmp/nyazo.png
link=`curl -s http://nyazo.ru/upload.php -F "id=''" -F "imagedata=@/tmp/nyazo.png"`
rm /tmp/nyazo.png
open $link
echo $link | pbcopy
```

Save it as something like `/usr/local/bin/nyazo` and make it executable (`chmod
+x <file>`). Then open Automator and create a new Service. Add the "Run Shell
Script" action to it, write `/usr/local/bin/nyazo -i` to it, and save it.

Click the "Run" button at the top right of the window to test the service works
as intended. A coordinates selecting cursor should appear. If you select a
region, it should be uploaded to Nyazo.

Now open "Keyboard" in "System Preferences". Go to the "Shortcuts" tab, and
select "Services" on the left. At the end of everything, you can bind the
interactive Nyazo Service you just created. I have it bound to
`Option-Cmd-Shift-4`.

Of course, you can also bind the fullscreen screenshot Nyazo to keyboard. Just
repeat the same steps, removing the "-i" option from the service, and binding to
something else.

Happy shooting!
