+++
date = "2017-06-28T16:02:11+01:00"
draft = false
title = "Remote development with Unison"

tags = ["Dropbox", "OS X", "Linux"]
categories = ["Tutorials"]

+++

# Use case

You have to run your program on a remote server. However your favourite editor
with your favourite configuration isn't available over SSH. Or its over-SSH mode
is very slow (looking at you, [TRAMP](https://www.gnu.org/software/tramp/)).
Also, you'd like to work online sometimes.

Unison is like Dropbox but without an intermediary server.

# Setting it up

First, you need to have two[^1] directories, one in your local machine and one in
your remote machine, that will be synchronised. For example,
`/local/path/to/my/software` in your local machine and `/remote/path/software`
in your remote server. You must have SSH access to the server, which is for
example at `user@host`.

Then you download and
install [Unison](http://www.cis.upenn.edu/~bcpierce/unison/docs.html).
Preferrably use your system's package. You need two executables: `unison` and
`unison-fsmonitor`. The second one is not always available with the `unison`
package: it is not there, for example on Ubuntu or OS X. To install it:

  * Ubuntu: install the `ocaml` package to compile
    Unison
    [from source](http://www.cis.upenn.edu/~bcpierce/unison/download.html). Then
    just run `make`. It might give an error, but with a little bit of luck you
    will have compiled the `unison-fsmonitor` executable before that. Copy it to
    your `$PATH`.
  * OS X: install [UNOX](https://github.com/hnsl/unox)
  
Now you have to set up a "profile" configuration file in your local machine. In
`$HOME/.unison/profile-name.prf`, write something like:

```
root = /local/path/to/my/software
root = ssh://user@host//remote/path/software

ignore = Regex ^(.*/)*[^/]*\.[^/]*$
ignorenot = Regex ^(.*/)*[^/]*\.(py|txt|md|ipynb|bash|gitignore)$
```

This will synchronize all files ending in `.py`, `.txt`, `.md`... or having no
"." in their filename, between the two directories mentioned at the beginning.
Read more about how the `ignore` and `ignorenot` rules
work
[here](http://www.cis.upenn.edu/~bcpierce/unison/download/releases/stable/unison-manual.html#ignore) and
[here](http://www.cis.upenn.edu/~bcpierce/unison/download/releases/stable/unison-manual.html#pathspec).

Finally, in your local machine, run:

```
unison profile-name -repeat watch -times -logfile /var/log/unison.log
```

And the directories shall be synchronized.

[^1]: Or more, but we will only set up two here.
