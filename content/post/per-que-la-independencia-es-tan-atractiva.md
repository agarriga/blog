+++
date = "2016-08-05T01:55:56+02:00"
draft = true
title = "Per què la independència és tan atractiva"

+++

Oju que jo vaig ser criat independentista, cantava les cançons d' "Història de Catalunya" sobre En Jaume I i En Ramon Berenguer IV, i vaig aprendre castellà a l'escola perquè els altres nens el parlaven. No em sento espanyol ni gota. Aquí no jutjo, simplement descric el procés que observo i els patrons.

Abans espanya ja "robava", però hi havia molts menys independentistes. Galícia gairebé no en té. Altres parts d'espanya parlent bastant diferent i ni n'han sentit a parlar, i a Suïssa ningú s'imagina de separar-se. La propaganda i el patriotisme ja porta un temps funcionant, i fins molt recentment no ha obtingut seguidors. Molts dels quals no tenen raó especial per estimar Catalunya més que ser el lloc on viuen, i podrien molt bé dir que consideren que és espanya! Aleshores, per què és tan guai ser indepe? Els mitjans de comunicació catalans s'ho mengen amb patates, i l'elit educada d'universitat també:


Propaganda: necessària per a estimular la llengua catalana, que si no seria morta. Argumentar que s'ha de matar una llengua ja no queda gaire bé.

- Conviccions polítiques diferents que la resta de l'estat: cagun els altres votants que m'impedeixen que guanyi qui jo vull
- Espanya és un país de merda, i distanciar-te'n dient que no és culpa teva i és un altre país és atractiu
  - L'establiment (govern ...) n'està a favor i per tant dóna estatus (potser això funcionava amb les elits? Oju que no explica com comença tot)
  
- Dóna un motiu per tenir esperança a gent que li va tot malament (o que percep que va tot malament perquè ho diuen els mitjans de comunicació): Quan hi hagi independència tindrem diners per això i allò

References
========
