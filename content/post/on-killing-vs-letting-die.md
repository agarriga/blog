+++
date = "2016-04-11T12:23:08+02:00"
draft = true
title = "On Doing vs. Allowing Harm"

categories = ["Non-mathematical truth-seeking"]
tags = ["Moral Philosophy", "Consequentialism"]
+++

I had heard of the excellent Stanford Encyclopedia of Philosophy before, but
never read an article from it. Recently however, after a productive discussion
with my flatmates about
[wild animal suffering](http://foundational-research.org/the-importance-of-wild-animal-suffering)
(which is also an
[interesting subject](https://robertwiblin.com/2010/01/21/civilizing-the-wilderness/)
that you may want to read on), I got recommended
[Doing vs. Allowing Harm](http://plato.stanford.edu/entries/doing-allowing/), by
Frances Howard-Snyder.

Before that, I had seldom (if ever) questioned consequentialism, and certainly
not recently. Regularly revising one's beliefs is important for the aim of rational
thinking, so i thought that reading about a distinction that, if relevant, would
render consequentialism false, would be interesting.

 [//]: # (to lo nu tcidu fi lo te drata noi va'o lo vajni gau ce jitfa fa la velzu'e sidbo cu cinri toi)

The article describes several ways of distinguishing between "doing harm" acts
and "allowing harm", proposed by different authors. Each explanation has
illustrating examples and breaking counter-examples, and the author dismisses
them all. The conclusion is rather interesting. It is proposed that not one, but several different distinctions are associated with doing or only allowing harm
is that the distinctions proposed so far
are completely arbitrary, and that there seems to be no reason to
