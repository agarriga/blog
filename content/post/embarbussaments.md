+++
date = "2016-03-31T12:38:39+02:00"
draft = false
title = "Embarbussaments"
tags = ["tongue-twisters", "catalan"]
categories = ["Language"]

+++

Attention! For those of you who do not understand Catalan, There is an [English
translation]({{< relref "#in-english">}}) below!

La meva germana i jo vam començar a fer la primera d'aquestes frases. No em
semblen especialment difícils de dir, però això potser és perquè els he escrit
jo. Vaig gaudir del procés i el resultat, i per això vaig escriure les altres
tres. Aquí van! Prova de dir-les!

- La mangosta Agustina i la llagosta Gustau, vaguistes, gasten en gustos a
  l'Agost.

<audio controls>
<source type="audio/mp4" src="/sounds/llagosta.m4a"/>
<source type="audio/ogg" src="/sounds/llagosta.ogg"/>
<source type="audio/mpeg" src="/sounds/llagosta.mp3"/>
</audio>

- Manca de xancles, xanques blanques i tanques de branques fan que tanquin
  encara la banca.

<audio controls>
<source type="audio/mp4" src="/sounds/xancles.m4a"/>
<source type="audio/ogg" src="/sounds/xancles.ogg"/>
<source type="audio/mpeg" src="/sounds/xancles.mp3"/>
</audio>

- La rara barana que el pare i la mare preparen fa cara la vara on para la Sara
  i s'encara al sarau i gatzara.

<audio controls>
<source type="audio/mp4" src="/sounds/sara.m4a"/>
<source type="audio/ogg" src="/sounds/sara.ogg"/>
<source type="audio/mpeg" src="/sounds/sara.mp3"/>
</audio>

- Fa poc que moc el soc del lloc al foc i toco un roc. Jo sóc un cóc de mocs que
  xoca al boc.

<audio controls>
<source type="audio/mp4" src="/sounds/roc.m4a"/>
<source type="audio/ogg" src="/sounds/roc.ogg"/>
<source type="audio/mpeg" src="/sounds/roc.mp3"/>
</audio>

Estan creades de forma que tenen una seqüència concreta de sons repetida moltes
vegades. Excepte la primera, també alternen síl·labes tòniques i àtones, per
donar ritme. Noteu també la diferència entre "o"s obertes [IPA ɔ] i tancades [o] en
l'última.

Traduir aquesta entrada m'ha fet adonar-me que, sí, les llengües romàniques i
l'anglès tenen [gairebé la mateixa gramàtica](http://esr.ibiblio.org/?p=6951).
Que fort.

## In English

Title: Tongue Twisters

My sister and I started composing the first of these sentences. They don't seem
hard to say to me, but that may be because I wrote them. I enjoyed the process
and results, so I wrote the other three. Here they go! Try to read them out
loud!

- La mangosta Agustina i la llagosta Gustau, vaguistes, gasten en gustos a
  l'Agost. *(Augustine (female) the mongoose and Gustaf the lobster, strikers,
spend on pleasures during August.)*

<audio controls>
<source type="audio/mp4" src="/sounds/llagosta.m4a"/>
<source type="audio/ogg" src="/sounds/llagosta.ogg"/>
<source type="audio/mpeg" src="/sounds/llagosta.mp3"/>
</audio>

- Manca de xancles, xanques blanques i tanques de branques fan que tanquin
  encara la banca. *(Lack of flipflops, white stilts and fences made of
  branches make the bank still be closing.)*

<audio controls>
<source type="audio/mp4" src="/sounds/xancles.m4a"/>
<source type="audio/ogg" src="/sounds/xancles.ogg"/>
<source type="audio/mpeg" src="/sounds/xancles.mp3"/>
</audio>

- La rara barana que el pare i la mare preparen fa cara la vara on para la Sara
  i s'encara al sarau i gatzara. *(The rare railing that father and mother
  prepare makes expensive the rod where Sara stops and turns to face the party and
  rejoicing.)*

<audio controls>
<source type="audio/mp4" src="/sounds/sara.m4a"/>
<source type="audio/ogg" src="/sounds/sara.ogg"/>
<source type="audio/mpeg" src="/sounds/sara.mp3"/>
</audio>

- Fa poc que moc el soc del lloc al foc i toco un roc. Jo sóc un cóc de mocs que
  xoca al boc. *(For a short time, I have been moving the stump from the place
  to the fire and touching a rock. I am a snot pie that shocks the buck.)*

<audio controls>
<source type="audio/mp4" src="/sounds/roc.m4a"/>
<source type="audio/ogg" src="/sounds/roc.ogg"/>
<source type="audio/mpeg" src="/sounds/roc.mp3"/>
</audio>

They are created to have a certain sequence of sounds repeated many times.
Except for the first one, they also alternate stressed and unstressed syllables,
to give rhythm. Note also the difference between open [IPA ɔ] and closed [o] "o"s in
the last one.

Translating this post made me realize that yes, latin-derived languages and
English have [almost the same grammar](http://esr.ibiblio.org/?p=6951). Damn.
