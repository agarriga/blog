+++
date = "2015-10-04T02:23:08+02:00"
draft = true
title = "Writeup: XXXXX The Mark of a Wizard"

categories = ["Algorithms"]
tags = ["SWERC"]
+++

This problem has an abysmal solving rate. Also, its inputs are really small, so
it seemed like it would be a nightmare to solve.

I was very much surprised when the solution came to me very much at once, in the
space of a few hours of not paying attention to class. Yet I subjectively think
the solution is pretty clever, so I'll explain it here.

The problem
========

You can read the whole problem text [here](). You may wish to skip reading it, as I will now explain the information we extract from the text.

First things first: the web of tunnels is a graph ([wikipedia]()). There are
nodes (tunnel intersections) and edges (tunnels). Furthermore, it can be thought
of as a directed graph: wizards will only take tunnels that go up, so all edges
have a direction. There are no horizontal tunnels. This is implicitly stated in
the "Input" section: "[...] an integer, u, that is the number of upward tunnels
from that point.". Since the H
