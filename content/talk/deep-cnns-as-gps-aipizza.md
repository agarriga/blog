+++
title = "AI+Pizza @ MSR: Deep CNNs as shallow GPs"
date = 2018-09-28T17:30:00Z
draft = false
event = "AI+Pizza series hosted by Microsoft Research Cambridge."

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []
categories = []

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

I gave a talk about my work ["Deep Convolutional Networks as shallow Gaussian Processes"](/publication/deep-cnns-as-gps/) as part of the AI+Pizza series hosted by Microsoft Research Cambridge.

The scheduled talk can be seen at https://talks.cam.ac.uk/talk/index/111097.
