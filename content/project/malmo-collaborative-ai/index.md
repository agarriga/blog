+++
title = "Malmö Collaborative AI Challenge"
date = 2017-06-01T01:34:27Z
draft = false

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Project summary to display on homepage.
summary = "Entry to the Malmö Collaborative AI Challenge, got 1st and 3rd places in different categories."

# Slides (optional).
#   Associate this page with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Optional external URL for project (replaces project detail page).
external_link = "https://www.microsoft.com/en-us/research/blog/malmo-collaborative-ai-challenge-winners/"

# Links (optional).
url_pdf = ""
url_code = "https://github.com/rhaps0dy/malmo-challenge"
url_dataset = ""
url_slides = "malmo-collaborative-ai.pd"
url_video = ""
url_poster = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{icon_pack = "fab", icon="twitter", name="Follow", url = "https://twitter.com"}]

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++
