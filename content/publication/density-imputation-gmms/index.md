+++
title = "Probability density imputation of missing data with Gaussian Mixture Models"
date = 2017-09-01T00:01:21Z
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Adrià Garriga-Alonso", "supervised by Mihaela van der Schaar"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = "Probability density imputation of missing data with Gaussian Mixture Models"
publication_short = "MSc Thesis"

# Abstract and optional shortened version.
abstract = "A method for filling in missing measurements in points in datasets, with some statistical assumptions, by drawing from the conditional density of the data as modelled by a Bayesian mixture of Gaussians.\n\nMultiple imputation (Rubin, 2004) is a procedure for conducting unbiased data analysis of an incomplete data set X, using complete-data analysis techniques. It consists in generating m imputed data sets in which the missing values Xmis have been replaced by plausible values, analysing the imputed data sets, and combining the results. To assert the soundness of this approach, the plausible values are assumed to be drawn from the posterior distribution of the missing values given the data, p(Xmis | X). Typically, m ≤ 10 samples are used.\n\nIn this thesis we go one step further, and set m → ∞. That is, we attempt to estimate the distribution p(Xmis | X) with a closed-form probability distribution, performing probability density imputation. To this end, we provide the Partially Observed Infinite Gaussian Mixture Model (POIGMM), an algorithm for (1) density estimation from incomplete data sets, and (2) density imputation; based on the Infinite GMM by Blei and Jordan (2006).\n\nA density-imputed data set can easily be reduced to a multiple-imputed data set, by taking m samples from the estimated posterior distribution. Accordingly, we compare the POIGMM with the state of the art in multiple imputation: MissForest (Stekhoven and Bühlmann, 2011) and MICE (Van Buuren and Oudshoorn, 1999)."
abstract_short = "A method for filling in missing measurements in points in datasets, with some statistical assumptions, by drawing from the conditional density of the data as modelled by a Bayesian mixture of Gaussians."

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Slides (optional).
#   Associate this page with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Digital Object Identifier (DOI)
doi = ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++
