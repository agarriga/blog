+++
title = "Deep Convolutional Networks as shallow Gaussian Processes"
date = 2019-01-16T00:12:44Z
draft = false

aliases = [
    "/publications/deep-cnns-as-gps/",
    "/convnets-as-gps/"
]


# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Adrià Garriga-Alonso", "Carl Edward Rasmussen", "Laurence Aitchison"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["1"]

# Publication name and optional abbreviated version.
publication = "Deep Convolutional Networks as shallow Gaussian Processes"
publication_short = "Accepted to ICLR"

# Abstract and optional shortened version.
abstract_short = "We show that the output of a CNN or ResNet with an appropriate prior over the parameters is a Gaussian process in the limit of infinitely many convolutional filters."
abstract = "We show that the output of a (residual) convolutional neural network (CNN) with an appropriate prior over the weights and biases is a Gaussian process (GP) in the limit of infinitely many convolutional filters, extending similar results [[1](https://arxiv.org/abs/1804.11271), [2](https://arxiv.org/abs/1711.00165)] for dense networks."

# Is this a selected publication? (true/false)
selected = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Slides (optional).
#   Associate this page with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = ""
url_preprint = "https://arxiv.org/abs/1808.05587"
url_code = "https://github.com/rhaps0dy/convnets-as-gps"
url_dataset = ""
url_project = ""
url_slides = "slides.odp"
url_video = ""
url_poster = "poster.pdf"
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Digital Object Identifier (DOI)
doi = ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++

For a convolutional network (CNN), the equivalent kernel can be computed exactly and, unlike "deep kernels", has very few parameters: only the hyperparameters of the original CNN. Further, we show that this kernel has two properties that allow it to be computed efficiently; the cost of evaluating the kernel for a pair of images is similar to a single forward pass through the original CNN with only one filter per layer. The kernel equivalent to a 32-layer ResNet obtains 0.84% classification error on MNIST, a new record for Gaussian processes with a comparable number of parameters.
