+++
title = "Solving Montezuma's Revenge with Planning and Reinforcement Learning"
date = 2016-08-17T00:32:09Z
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Adrià Garriga-Alonso", "supervised by Anders Jonsson"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference paper
# 2 = Journal article
# 3 = Manuscript
# 4 = Report
# 5 = Book
# 6 = Book section
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = "Solving Montezuma's Revenge with Planning and Reinforcement Learning"
publication_short = "BSc Thesis"

# Abstract and optional shortened version.
abstract = "How much domain knowledge do a SoTA planning algorithm, and a basic RL algorithm, need to solve Montezuma's Revenge, the benchmark for sparse rewards in RL? It turns out that a few simple tweaks do it, but we speculate that it's not straightforward to do equivalent tweaks automatically for other games.\n\nTraditionally, methods for solving Sequential Decision Processes (SDPs) have not worked well with those that feature sparse feedback. Both planning and reinforcement learning, methods for solving SDPs, have trouble with it. With the rise to prominence of the Arcade Learning Environment (ALE) in the broader research community of sequential decision processes, one SDP featuring sparse feedback has become familiar: the Atari game Montezuma’s Revenge. In this particular game, the great amount of knowledge the human player already possesses, and uses to find rewards, cannot be bridged by blindly exploring in a realistic time. We apply planning and reinforcement learning approaches, combined with domain knowledge, to enable an agent to obtain better scores in this game. We hope that these domain-specific algorithms can inspire better approaches to solve SDPs with sparse feedback in general."
abstract_short = "How much domain knowledge do a SoTA planning algorithm, and a basic RL algorithm, need to solve Montezuma's Revenge, the benchmark for sparse rewards in RL? It turns out that a few simple tweaks do it, but we speculate that it's not straightforward to do equivalent tweaks automatically for other games."

# Is this a selected publication? (true/false)
selected = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["deep-learning"]` references 
#   `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects = []

# Slides (optional).
#   Associate this page with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = ""

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = []

# Links (optional).
url_pdf = "https://repositori.upf.edu/bitstream/handle/10230/30867/Garriga_2016.pdf?sequence=1&isAllowed=y"
url_preprint = ""
url_code = "https://github.com/rhaps0dy/solving-mr-planning-rl/"
url_dataset = ""
url_project = ""
url_slides = "slides.pdf"
url_video = "https://www.youtube.com/watch?v=KSPYzLE0uy8"
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
# url_custom = [{name = "Custom Link", url = "http://example.org"}]

# Digital Object Identifier (DOI)
doi = ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
[image]
  # Caption (optional)
  caption = ""

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = ""
+++
