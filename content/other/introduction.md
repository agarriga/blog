+++
date = "2015-12-01T01:45:18+01:00"
draft = true
title = "Introduction for LW Mastermind"

+++
Hello everyone! My name is Adrià Garriga Alonso, my screen name is
rhaps0dy. I am 20 years old and am currently studying the final year
of my bachelor's in Computer Science. I like computer science and
doing, not so much watching, sports! Currently I do Tae Kwon Do. I
used to play Water Polo and the violin.

I set my goals with the help of ELiot’s
[list of common human goals](http://lesswrong.com/r/discussion/lw/mnz/list_of_common_human_goals/). You
may want to check it out.

Long-term goals (in order of importance)
-------------------
1. Everlasting health.
   * Get a better diet (current is not far from optimal though)
   * Maintain an appropriate level of exercise
   * Sign up for cryonics!
2. Avoid pain, hunger, cold and loneliness.
3. Sociality. Have interesting people to talk to, friendship.
4. Continuously improve my skills, in general.
5. Leave a mark in the world, a legacy.
   * Via Artificial Intelligence research, but that may change in the future.
	 *  Publish my first original discovery in a journal
6. Travel around, have some new experiences. This one is almost an afterthought.

Short-term subgoals, projects (not in order)
--------------------
1. Fix my procrastination, increase productivity.
2. Devise or otherwise acquire an optimal diet. Maybe include Soylent.
3. Keep learning Mandarin.
4. Keep practising programming problems. Increase my Codeforces rating.
5. Keep training in a martial art.
6. Complete my bachelor's thesis with a 10/10 grade (achievable if I work hard).
7. Pass my current university courses with a grade above my average.
8. Finish organizing everything for my Erasmus stay in Karlstad
   University. This choice of university does not align with my goals
   very well, but I chose it almost two years ago.
9. Get accepted to at least one of my universities of choice for MSc in AI.
10. Figure out what subfield of AI I wish to investigate


Observations
---------
Long-term goals 1, 2, 3 and 4 are already fulfilled. It is however important to
me that they remain fulfilled, forever.

Short-term subgoals 3 and 4 are included in goal 4, but are mostly done for fun. Maybe I should add a goal of "fun"?


Organizational
-----------
I'm from near Barcelona, Catalonia, Spain. Thus, my time is UTC+1. I
have an irregular schedule, as exam week starts, then I’m out of town
for a while, then are holidays, then I’m out again, ... However, if
the meetings are in Asia&Australia afternoon-evening it will likely be
good for me.




