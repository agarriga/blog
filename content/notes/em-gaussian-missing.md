+++
date = "2017-07-13T23:35:22+01:00"
draft = true
title = "Expectation Maximisation on Gaussian Mixtures with Missing Data"
tags = ["MSc thesis"]
categories = []
+++

# M step
We have $\tau$, a `[n_samples, n_components]`-shape matrix that gives us the
responsibilities of each Gaussian component for each sample. We index it as
$\tau\_{ik}$ with $i$ (sample) and $k$ (component). We also have $x$ indexed as
$x\_{ij}$, the matrix of observations, with shape `[n_samples, n_features]`. Some
of them are missing. We write $\textit{obs}^i$ to mean the indices of the observed
variables in the $i$th sample, and $y\_{i,\text{obs}} := y\_{i,\text{obs}^i}$ to
mean the observed values of the $i$th sample. Same with missing values and $\textit{mis}^i$.

Now we need to calculate:

$$ \mu\_{kj}^{(t+1)} = \frac{1}{\sum\_{i=1}^N \tau\_{ik}} \mathbb{E}\left[
\sum\_{i=1}^N \tau\_{ik} y\_{ij} \middle| y\_{i,\text{obs}}, \theta\_k^{(t)}\right] =
\frac{1}{\sum\_{i=1}^N \tau\_{ik}} \sum\_{i=1}^N\tau\_{ik}
\mathbb{E}\left[y\_{ij} | y\_{i,\text{obs}},\,\mu\_k^{(t)} \right] $$

Now $\mathbb{E}\left[y\_{ij} | y\_{i,\text{obs}},\,\mu\_k^{(t)} \right]$  is $y\_{ij}$ if the value is observed, and
$\mu\_{jk | y\_{i,\text{obs}}}^{(t)}$ otherwise. The latter
is an element of the vector $\mu\_{k | y\_{i,\text{obs}}}^{(t)}$
which is computed as follows: (we drop the component $k$ and the time $t$ for
clarity):

$$ \mu_{|y\_{i,\text{obs}}} = \mu\_{\text{mis}^i} +
\Sigma\_{\text{mis}^i,\text{obs}^i} {\Sigma\_{\text{obs}^i,\text{obs}^i}}^{-1}
(y\_{\text{obs}^i} - \mu\_{\text{obs}^i})$$

We also need to calculate the covariance, which is:

$$ \Sigma\_{kjj'}^{(t+1)} = \frac{1}{\sum\_{i=1}^N \tau\_{ik}} \mathbb{E}\left[
\sum\_{i=1}^N \tau\_{ik} \left(y\_{ij} - \mu\_{kj}^{(t+1)}\right)
\left(y\_{ij'} - \mu\_{kj'}^{(t+1)}\right) \middle| y\_{i,\text{obs}},
\theta\_k^{(t)}\right] $$
$$ = \frac{1}{\sum\_{i=1}^N \tau\_{ik}} \sum\_{i=1}^N \tau\_{ik} \mathbb{E}\left[
\left(y\_{ij} - \mu\_{kj}^{(t+1)}\right)
\left(y\_{ij'} - \mu\_{kj'}^{(t+1)}\right) \middle| y\_{i,\text{obs}},
\theta\_k^{(t)}\right]$$
$$= \frac{1}{\sum\_{i=1}^N \tau\_{ik}} \sum\_{i=1}^N \tau\_{ik} \mathbb{E}\left[
y\_{ij}
y\_{ij'}
\middle| y\_{i,\text{obs}},
\theta\_k^{(t)}\right]  -  \mu\_{kj}^{(t+1)}\mu\_{kj'}^{(t+1)} $$

And since $y\_{ij}$ and $y\_{ij'}$ are independent given $y\_{i,\text{obs}}$ and
$\theta_k^{(t)}$, we can just apply $\mathbb{E}[xy] = \mathbb{E}[x]\mathbb{E}[y]$:

$$\Sigma\_{kjj'}^{(t+1)} = \frac{1}{\sum\_{i=1}^N \tau\_{ik}} \sum\_{i=1}^N
\tau\_{ik} \mathbb{E}\left[ y\_{ij} \middle| y\_{i,\text{obs}},
\theta\_k^{(t)}\right] \mathbb{E}\left[ y\_{ij'} \middle| y\_{i,\text{obs}},
\theta\_k^{(t)}\right]- \mu\_{kj}^{(t+1)}\mu\_{kj'}^{(t+1)} $$

We now show that $y\_{ij}$ and $y\_{ij'}$ are effectively independent.
## Expectation of square of two variables from a joint Gaussian

Suppose we draw two variables from a joint Gaussian distribution:
$$\begin{Bmatrix} y\_1 &  b \\ y\_2 & a \end{Bmatrix}$$
